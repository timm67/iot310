from picamera import PiCamera
from time import sleep
import time

# Init the camera
camera = PiCamera()

# Set rotation
camera.rotation = 270

# Enable a preview (only works if screen is attached)
camera.start_preview()

# Wait for light to adjust
sleep(3)

# Set a timestamp
timestamp = int(time.time())

# Add a filename with a timestamp
filename = "img_{}.jpg".format(str(timestamp))

# Take the photo and output to file
camera.capture(filename)

# Disable preview
camera.stop_preview()

print 'File has been stored as: {}'.format(filename)
