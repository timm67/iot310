from picamera import PiCamera
from time import sleep
import time

# Init camera
camera = PiCamera()

# Set rotation (in degrees) for camera
camera.rotation = 270

# Start preview screen
camera.start_preview()

# Add a filename with a timestamp
timestamp = int(time.time())

# Setup filename
filename = "video_{}.h264".format(str(timestamp))

# Start recording (streaming to file)
camera.start_recording(filename)

# Duration to record
sleep(10)

# Stop recording
camera.stop_recording()

# End preview screen
camera.stop_preview()

print 'File has been stored as: {}'.format(filename)