from picamera import PiCamera
from time import sleep
import time

#  Init camera
camera = PiCamera()

#  Start preview
camera.start_preview()

# Uncomment to set rotation
# camera.rotation = 180

#  Duration of preview
sleep(20)

#  Stop preview
camera.stop_preview()