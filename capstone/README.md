## IOT310 Capstone: Is-it-Tim using ML deployed to Azure Edge ##

The Capstone project shown below is based on class 8. While a lot of the 
files were derived from the docker containers that were assembled by the 
instructor, the deployment.json and other files were specifically modified
for this project. 

There are three artifacts shown below. The first is the output of the model
showing correlations between the captured image and models that the ML has 
been provided. 

![](./assets/sensor-setup.jpg)


The second image (above) provides a simple picture of the test fixture.
The third provides a short movie of me sitting in front of camera and then 
moving out of the way to get a red 'x'. 

```
Eluating image with model model
0
category:  {0: 'Tim\n'}
result:  {'class': 'Tim', 'confidence': '3.2009672e-07'}
172.18.0.4 - - [03/Jun/2018 06:52:41] "POST /classify HTTP/1.1" 200 -
I'm here
{'class': 'Tim', 'confidence': '3.2009672e-07'}
http://172.18.0.4:8082/no
Evaluating image with model model
0
category:  {0: 'Tim\n'}
result:  {'class': 'Tim', 'confidence': '0.99623704'}
172.18.0.4 - - [03/Jun/2018 06:52:46] "POST /classify HTTP/1.1" 200 -
I'm here
{'class': 'Tim', 'confidence': '0.99623704'}
http://172.18.0.4:8082/yes
Evaluating image with model model
0
category:  {0: 'Tim\n'}
result:  {'class': 'Tim', 'confidence': '0.99995315'}
172.18.0.4 - - [03/Jun/2018 06:52:52] "POST /classify HTTP/1.1" 200 -
I'm here
{'class': 'Tim', 'confidence': '0.99995315'}
http://172.18.0.4:8082/yes
Evaluating image with model model
0
```

Here's the video: 

![](./assets/VID_20180604_202707.mp4)